Name:           python-merge3
Version:        0.0.13
Release:        %autorelease
Summary:        Python implementation of 3-way merge

# Check if the automatically generated License and its spelling is correct for Fedora
# https://docs.fedoraproject.org/en-US/packaging-guidelines/LicensingGuidelines/
License:        GPL-2.0-or-later
URL:            https://www.breezy-vcs.org/
Source:         %{pypi_source merge3}

BuildArch:      noarch
BuildRequires:  python3-devel


# Fill in the actual package description to submit package to Fedora
%global _description %{expand:
A Python implementation of 3-way merge of texts.}


%description %_description

%package -n     python3-merge3
Summary:        %{summary}

%description -n python3-merge3 %_description


%prep
%autosetup -p1 -n merge3-%{version}


%generate_buildrequires
%pyproject_buildrequires


%build
%pyproject_wheel


%install
%pyproject_install
# For official Fedora packages, including files with '*' +auto is not allowed
# Replace it with a list of relevant Python modules/globs and list extra files in %%files
%pyproject_save_files '*' +auto


%check
%pyproject_check_import


%files -n python3-merge3 -f %{pyproject_files}


%changelog
%autochangelog